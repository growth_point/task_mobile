import { Component, OnDestroy, OnInit } from '@angular/core';
import { IonicPage, ItemSliding, NavController } from 'ionic-angular';

import { Project } from '../../providers/projects/projects';
import { FiltersService, ProjectsService } from '../../providers/providers';
import { Label } from '../../providers/user/user';

import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Storage } from "@ionic/storage";

@IonicPage()
@Component({
  selector: 'page-home',
  template: `
    <ion-header>
      <ion-navbar>
        <button ion-button menuToggle>
          <ion-icon name="menu"></ion-icon>
        </button>
    
        <ion-title>Today</ion-title>
        
        <ion-buttons end>
          <button ion-button clear *ngIf="user" (click)="openProfile()">
            {{ user.name }}
          </button>
        </ion-buttons>
        
      </ion-navbar>
    </ion-header>

    <ion-content>
      <ion-list>
        <ion-item text-wrap (click)="today(p)">
          <h3>Today</h3>
        </ion-item>
      </ion-list>
      <br />
      <projects-list></projects-list>
    </ion-content>
  `,
})
export class HomePage {
  constructor(public filters: FiltersService,
              public nav: NavController) {
  }

  today() {
    this.filters.chooseLabel(1);
    this.nav.push("TasksPage")
  }

  openProfile() {

  }
}

@Component({
  selector: 'projects-list',
  template: `
    <ion-list>
      <ion-item-sliding *ngFor="let p of projects"  #slidingItem>
        <ion-item text-wrap (click)="openProject(p)">
          <h3>{{p.title}}</h3>
        </ion-item>
        
        <ion-item-options side="right">
          <button ion-button class="edit" color="default" (click)="editProject(p, slidingItem)">
            <ion-icon icon-only name="create"></ion-icon>
            Edit
          </button>
          <button ion-button class="done" color="secondary" (click)="doneProject(p, slidingItem)">
            <ion-icon icon-only name="checkmark"></ion-icon>
            Done
          </button>
        </ion-item-options>
      </ion-item-sliding>
      
      <ion-item>
        <button ion-button class="create" (click)="createProject()">
          <ion-icon name="add"></ion-icon>
          <span>Create Project</span>
        </button>
      </ion-item>
    </ion-list>
  `,
})
export class ProjectList implements OnInit, OnDestroy {

  labels: Label[] = null;
  projects: Project[] = null;

  constructor(public filtersService: FiltersService,
              public projectsService: ProjectsService,
              public navCtrl: NavController,
              public storage: Storage) {
  }

  ngOnInit() {
    this.projectsService.all().subscribe(projects => {
      this.projects = projects.filter(i => !i.completed_at)
    });
  }

  ngOnDestroy() {
  }

  createProject() {
    this.navCtrl.push(ProjectCreatePage);
  }

  doneProject(project: Project, slidingItem: ItemSliding) {
    this.projectsService.done(project);
    slidingItem.close();
  }

  editProject(project: Project, slidingItem: ItemSliding) {
    // this.nav.push(ProjectEditPage);
    slidingItem.close();
  }

  openProject(project: Project) {
    this.filtersService.chooseProject(project.id);
    this.navCtrl.push('TasksPage');
  }
}

@Component({
  selector: 'project-create-page',
  template: `
    <ion-header>
      <ion-navbar>
        <ion-title>
          Create Project
        </ion-title>
      </ion-navbar>
    </ion-header>

    <ion-content>
      <form [formGroup]="form" (ngSubmit)="save()">
        <ion-item>
          <ion-label>Title: </ion-label>
          <ion-input id="project-title" type="text" autofocus clearInput max="255" formControlName="title"></ion-input>
        </ion-item>
        <button ion-button type="submit" [disabled]="!form.valid">Create Project</button>
      </form>
    </ion-content>
  `,
})
export class ProjectCreatePage {
  formVisible: boolean;

  private form: FormGroup;

  constructor(private formBuilder: FormBuilder, public projectsService: ProjectsService) {
    this.form = this.formBuilder.group({
      title: ['', Validators.required],
    });
  }

  save() {
    this.projectsService.add(this.form.value as Project);
    this.formVisible = false;
    this.form.reset();

  }
}

