import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';

import { HomePage, ProjectCreatePage, ProjectList } from "./home";
import { FiltersService, ProjectsService, LabelsService } from "../../providers/providers";

@NgModule({
  declarations: [
    HomePage,
    ProjectList,
    ProjectCreatePage,
  ],
  imports: [
    IonicPageModule.forChild(HomePage),
    // TranslateModule.forChild()
  ],
  exports: [
    HomePage,
    ProjectList,
    ProjectCreatePage,
  ],
  providers: [
    LabelsService,
    FiltersService,
    ProjectsService,
  ]
})
export class HomePageModule {
}
