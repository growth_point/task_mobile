import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { ProjectList } from './home';

describe('BannerComponent (inline template)', () => {

  let comp: ProjectList;
  let fixture: ComponentFixture<ProjectList>;
  let de: DebugElement;
  let el: HTMLElement;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ProjectList], // declare the test component
    });

    fixture = TestBed.createComponent(ProjectList);

    comp = fixture.componentInstance; // BannerComponent test instance

    // query for the title <h1> by CSS element selector
    de = fixture.debugElement.query(By.css('h1'));
    el = de.nativeElement;
  });
});