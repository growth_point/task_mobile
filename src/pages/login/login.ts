import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, ToastController } from 'ionic-angular';

import { UserService } from '../../providers/providers';
import { Token } from "../../providers/api/token";

@IonicPage()
@Component({
  selector: 'page-login',
  template: `
  <ion-header>

    <ion-navbar>
      <ion-title>Login</ion-title>
    </ion-navbar>
  
  </ion-header>
  
  
  <ion-content>
    <div *ngIf="loading">...</div>
    <form *ngIf="!loading" (submit)="doLogin()">
      <ion-list>
  
        <ion-item>
          <ion-label fixed>Username</ion-label>
          <ion-input type="input" [(ngModel)]="account.username" name="username"></ion-input>
        </ion-item>
  
        <!--
        Want to use a Username instead of an Email? Here you go:
  
        <ion-item>
          <ion-label floating>{{ 'USERNAME' | translate }}</ion-label>
          <ion-input type="text" [(ngModel)]="account.username" name="username"></ion-input>
        </ion-item>
        -->
  
        <ion-item>
          <ion-label fixed>password</ion-label>
          <ion-input type="password" [(ngModel)]="account.password" name="password"></ion-input>
        </ion-item>
  
        <div padding>
          <button ion-button color="primary" block>Login</button>
        </div>
  
      </ion-list>
    </form>
  </ion-content>
  `
})
export class LoginPage implements OnInit {
  loading: boolean = true;

  // The account fields for the login form.
  // If you're using the username field with or without email, make
  // sure to add it to the type
  account: { username: string, password: string } = {
    username: 'john.doe',
    password: 'jwtpass'
  };

  // Our translated text strings
  private loginErrorString: string;

  constructor(public user: UserService,
              public tokenService: Token,
              public nav: NavController,
              public toastCtrl: ToastController) {
  }

  ngOnInit() {
    this.tokenService.has().then(has => {
      if (has) {
        this.nav.push('HomePage');
        return;
      }

      this.loading = false;
    });
    // this.page.set('task');
  }

// Attempt to login in through our UserService service
  doLogin() {
    //this.account
    this.user.login(this.account.username, this.account.password).subscribe((resp) => {
      this.nav.push('HomePage');
    }, (err) => {
      // this.nav.push('HomePage');

      // Unable to log in
      let toast = this.toastCtrl.create({
        message: this.loginErrorString,
        duration: 3000,
        position: 'top'
      });
      toast.present();
    });
  }
}
