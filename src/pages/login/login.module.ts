import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';

import { LoginPage } from './login';
import { Api, UserService } from "../../providers/providers";

@NgModule({
  declarations: [
    LoginPage,
  ],
  imports: [
    IonicPageModule.forChild(LoginPage),
    // TranslateModule.forChild()
  ],
  exports: [
    LoginPage
  ],
  providers: [
    Api,
    UserService,
  ]
})
export class LoginPageModule {
}
