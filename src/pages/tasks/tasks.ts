import { Component, OnDestroy, OnInit } from '@angular/core';
import { TasksService, ProjectsService, FiltersService, LabelsService } from "../../providers/providers";
import { Project } from "../../providers/projects/projects";
import { Task } from "../../providers/tasks/tasks";
import { Storage } from "@ionic/storage";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { IonicPage, NavController } from "ionic-angular";
import { Label } from "../../providers/labels/labels";

@IonicPage()
@Component({
  selector: 'page-tasks',
  template: `
    <ion-header>
      <ion-navbar>
        <ion-title *ngIf="project">
          {{project.title}}
        </ion-title>
        <ion-title *ngIf="label">
          {{label.title}}
        </ion-title>
      </ion-navbar>
    </ion-header>

    <ion-content>
      <ion-list>
        <ion-item-sliding *ngFor="let t of tasks">
          <ion-item text-wrap (click)="open(t)">
            <h3>
              <button ion-button clear (click)="done(t)">
                <ion-icon float-start padding icon-only [name]="t.completed_at ? 'checkmark-circle' : 'checkmark-circle-outline'"></ion-icon>
              </button>
              {{t.title}}
            </h3>
            <ul class="labels">
              <li *ngFor="let label of getLabels(t.id)" >
                  <ion-badge>{{label.title}}</ion-badge>
              </li>
            </ul>
          </ion-item>
        
          <ion-item-options side="right">
            <button ion-button color="default" (click)="edit(t)">
              <ion-icon icon-only name="pulse"></ion-icon>
              Today
            </button>
          </ion-item-options>
        </ion-item-sliding>
        
        <ion-item>
          <button ion-button class="create" (click)="createTask()">
            <ion-icon name="add"></ion-icon>
            <span>Create Task</span>
          </button>
        </ion-item>
      </ion-list>
      
    </ion-content>
  `,
})
export class TasksPage implements OnInit, OnDestroy {

  project: Project;
  label: Label;

  labels: Map<number, Label[]> = new Map;

  tasks: Task[];

  constructor(public tasksService: TasksService,
              public projectsService: ProjectsService,
              public labelsService: LabelsService,
              public filtersService: FiltersService,
              public nav: NavController,
              public storage: Storage) {
  }

  ngOnDestroy() {
  }

  ngOnInit() {
    this.tasksService.all().subscribe(tasks => {
      this.filtersService.current().subscribe(filters => {

        if (filters.projectId) {
          this.projectsService.all()
            .subscribe(projects => this.project = projects.filter(i => i.id == filters.projectId)[0]);

          this.tasks = tasks.filter(i => i.project_id == filters.projectId);

          return;
        }

        this.labelsService.all()
          .subscribe(labels => {
            if (filters.labelId) {
              this.label = labels.filter(i => i.id == filters.labelId)[0];
            }

            this.labels = new Map;
            tasks.forEach((task: Task) => {
              labels.forEach(label => {
                if (task.labels.includes(label.id)) {
                  if (!(this.labels[task.id] instanceof Array)) {
                    this.labels[task.id] = [label];
                  } else {
                    this.labels[task.id].push(label);
                  }
                }
              })
            });
          });

        if (filters.labelId) {
          this.tasks = tasks.filter(i => {
            return i.labels.includes(filters.labelId)
          });

          return;
        }
      });
    });
  }

  getLabels(taskId: number): Label[] {
    let labels = this.labels[taskId];
    if (!labels) {
      return []
    }

    return labels
  }

  // TODO: optimize and remove this method
  labelsTitleById(labels: Label[], ids: number[]): string[] {
    let labelsTitle: string[] = [];
    labels.filter(i => ids.includes(i.id)).forEach(i => {
      labelsTitle.push(i.title);
    });

    return labelsTitle;
  }

  open() {

  }

  createTask() {
    this.nav.push(TaskCreatePage);
  }

  done(task: Task) {
    this.tasksService.done(task);
  }

  edit(task: Task) {

  }

}


@Component({
  selector: 'task-create-page',
  template: `
    <ion-header>
      <ion-navbar>
        <ion-title>
          Create Task
        </ion-title>
      </ion-navbar>
    </ion-header>

    <ion-content>
      <form [formGroup]="form" (ngSubmit)="save()">
        <ion-item>
          <ion-label>Title: </ion-label>
          <ion-input type="text" id="task-title" autofocus clearInput max="255" formControlName="title"></ion-input>
        </ion-item>
        <button ion-button type="submit" [disabled]="!form.valid">Create Task</button>
      </form>
    </ion-content>
  `,
})
export class TaskCreatePage {
  formVisible: boolean;

  private form: FormGroup;

  constructor(private formBuilder: FormBuilder, public tasksService: TasksService) {
    this.form = this.formBuilder.group({
      title: ['', Validators.required],
    });
  }

  save() {
    this.tasksService.add(this.form.value as Task);
    this.formVisible = false;
    this.form.reset();

  }
}


