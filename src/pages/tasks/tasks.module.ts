import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';

import { TaskCreatePage, TasksPage } from "./tasks";
import { ProjectsService , TasksService, LabelsService, FiltersService } from "../../providers/providers";

@NgModule({
  declarations: [
    TasksPage,
    TaskCreatePage
  ],
  imports: [
    IonicPageModule.forChild(TasksPage),
    // TranslateModule.forChild()
  ],
  exports: [
    TasksPage,
    TaskCreatePage,
  ],
  providers: [
    FiltersService,
    ProjectsService,
    TasksService,
    LabelsService
  ]
})
export class TasksPageModule {
}
