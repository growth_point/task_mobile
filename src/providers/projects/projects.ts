import { Injectable, OnDestroy } from '@angular/core';

import { Api, Link } from '../api/api';
import { Task } from '../tasks/tasks';
import { BehaviorSubject } from "rxjs/BehaviorSubject";
import { Storage } from "@ionic/storage";

export type Project = {
    id: number;
    title: string;
    tasks: Task[];
    completed_at?: Date;
    _links: {
        self: Link;
        project: Link;
        tasks: Link;
        user: Link;
    }
}

@Injectable()
export class ProjectsService implements OnDestroy {
    private $projects = new BehaviorSubject<Project[]>([]);
    private projects: Project[] = [];
    private syncTimeout = 30000;
    private intervalId: number;
    private tmpId = 0;

    constructor(public api: Api,
                public storage: Storage) {
    }

    ngOnDestroy() {
        clearInterval(this.intervalId);
    }

    private fromStorage(list: Project[]) {
        this.projects = list;
        this.$projects.next(this.projects);
    }

    private fromServer(list: Project[]) {
        //TODO: must be merge logic here
        this.projects = list;
        this.broadcast();
    }

    private findAndReplace(id: number, project: Project) {
        this.projects[ this.projects.findIndex(i => i.id == id) ] = project;
        this.broadcast();
    }

    private broadcast() {
        this.$projects.next(this.projects);
        this.storage.set('projects', this.projects);
    }

    all(): BehaviorSubject<Project[]> {

        if (this.projects.length > 0) {
            return this.$projects;
        }

        this.storage.get('projects').then(res => {
            if (!res) {
                this._sync();
                return;
            }

            this.fromStorage(res);
        });

        if (!this.intervalId) {
            this.intervalId = setInterval(this._sync.bind(this), this.syncTimeout);
        }

        return this.$projects;
    }

    private _sync() {
        type ProjectsResponse = {
            _embedded: {
                projects: Project[];
            };
            _links: {
                self: Link;
                profile: Link;
            };
        }

        this.api.get<ProjectsResponse>('projects')
            .retry(5)
            .subscribe(resp => {
                if (resp.body._embedded.projects instanceof Array) {
                    this.fromServer(resp.body._embedded.projects);
                }
            });
    }

    add(project: Project) {
        let tmpId = this.tmpId--;
        project.id = tmpId;

        this.findAndReplace(tmpId, project);

        type Response = {
            project: Project
        }
        this.api.post<Response>('project', {'project': project}).subscribe(resp => {
            this.findAndReplace(tmpId, resp.body.project);
        });
    }

    update(project: Project) {
        this.findAndReplace(project.id, project);

        //TODO: project may have negative Id. Need to sync it in background?
        type Response = {
            project: Project
        }
        this.api.put<Response>('project', {'project': project}).subscribe(resp => {
            this.findAndReplace(project.id, resp.body.project);
        });
    }

    done(project: Project) {
        this.projects.find(i => i.id == project.id).completed_at = new Date;
        this.broadcast();

        type Response = {}
        this.api.post<Response>('project/' + project.id + '/done', {})
            .subscribe()
    }

}
