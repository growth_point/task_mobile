import { Injectable } from '@angular/core';
import { Storage } from "@ionic/storage";
import { Subject } from "rxjs/Subject";

@Injectable()
export class Page {
  private $page: Subject<string> = new Subject<string>();
  private page: string = null;

  constructor(public storage: Storage) {
    storage.ready().then(() => {
      storage.get('page').then(page => {
        if (this.page) {
          return; // then page already changed somehow, don't override
        }

        this.page = page;
        this.$page.next(page)
      });
    });
  }

  public set(val: string) {
    this.storage.set('page', val);
  }

  public get(): Subject<string> {
    return this.$page;
  }


}