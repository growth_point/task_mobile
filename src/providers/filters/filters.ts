import { Injectable } from '@angular/core';
import { BehaviorSubject } from "rxjs/BehaviorSubject";
import { Storage } from "@ionic/storage";

type Filters = {
  projectId?: number;
  labelId?: number;
}

@Injectable()
export class FiltersService {
  private $filters = new BehaviorSubject<Filters>({});
  private filters: Filters = null;

  constructor(public storage: Storage) {
  }

  private choose(val: Filters) {
    this.filters = val;
    this.$filters.next(this.filters);
    this.storage.set('filters', this.filters);
  }


  chooseProject(projectId: number) {
    this.choose({projectId: projectId});
  }

  chooseLabel(labelId: number) {
    this.choose({labelId: labelId});
  }

  reset() {
    this.choose({});
  }

  current(): BehaviorSubject<Filters> {
    if (this.filters == null) {
      this.filters = {};
      this.$filters.next(this.filters);
      this.storage.get('filters').then(res => {
        if (res != null) {
          this.choose(res);
        }
      });
    }

    return this.$filters;
  }
}