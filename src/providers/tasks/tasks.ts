import { Injectable } from '@angular/core';

import { Api, Link } from '../api/api';
import { BehaviorSubject } from "rxjs/BehaviorSubject";
import { Storage } from "@ionic/storage";

export type Task = {
    id: number;
    title: string;
    project_id: number;
    completed_at?: Date;
    labels: number[];
    _links: {
        self: Link;
        project: Link;
        user: Link;
        labels: Link;
    }
}


@Injectable()
export class TasksService {
    private $tasks = new BehaviorSubject<Task[]>([]);
    private tasks: Task[] = [];
    private intervalId: number;
    private syncTimeout = 30000;
    private tmpId = 0;

    constructor(public api: Api,
                public storage: Storage) {
    }

    private fromStorage(tasks: Task[]) {
        this.tasks = tasks;
        this.$tasks.next(this.tasks);
    }

    private fromServer(tasks: Task[]) {
        this.tasks = tasks;
        this.$tasks.next(this.tasks);
        this.storage.set('tasks', this.tasks);
    }

    private findAndReplace(id: number, task: Task) {
        this.tasks[ this.tasks.findIndex(i => i.id == id) ] = task;
        this.$tasks.next(this.tasks);
        this.storage.set('projects', this.tasks);
    }

    private find(id: number): Task {
        return this.tasks.find(t => t.id == id)
    }

    all(): BehaviorSubject<Task[]> {
        if (this.tasks.length > 0) {
            return this.$tasks;
        }

        this.storage.get('tasks').then(res => {
            if (!res) {
                this._sync();
                return;
            }

            this.fromStorage(res);
        });

        if (!this.intervalId) {
            this.intervalId = setInterval(this._sync.bind(this), this.syncTimeout);
        }

        return this.$tasks;
    }

    private _sync() {
        type TasksResponse = {
            _embedded: {
                tasks: Task[];
            };
            _links: {
                self: Link;
                profile: Link;
            };
        }

        this.api.post<TasksResponse>('tasks', {'timestamp': (new Date).toTimeString()})
            .subscribe(resp => {
                this.fromServer(resp.body._embedded.tasks);
            });
    }

    add(task: Task) {
        task.id = this.tmpId--;
        this.tasks.push(task);
        this.$tasks.next(this.tasks);

        type Response = {
            task: Task
        }
        this.api.post<Response>('task', {'task': task}).subscribe(resp => {
            this.findAndReplace(task.id, resp.body.task);
        });
    }

    done(task: Task) {
        this.find(task.id).completed_at = new Date;

        type Response = {}
        this.api.post<Response>('task/' + task.id + '/done', {})
            .subscribe()
    }

}
