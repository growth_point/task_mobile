import { Injectable } from "@angular/core";
import { Storage } from "@ionic/storage";

@Injectable()
export class Token {
    protected token: string;
    protected tokenLoad: Promise<string>;

    constructor(public storage: Storage) {
        if (this.token) {
            storage.set('access_token', this.token);
        } else {
            this.storage.get('access_token').then(token => {
                if (token) {
                    this.token = token;
                }
            });
        }
    }


    public has(): Promise<boolean> {
        return new Promise(resolve => {
            if (this.token) {
                resolve(true);
                return;
            }
            this.get().then(token => {
                if (token) {
                    // resolve(true);
                    // return;
                }

                resolve(false);
            })
        });
    }

    public get(): Promise<string> {
        return new Promise(resolve => {
            if (this.token) {
                resolve(this.token);
                return;
            }
            this.storage.get('access_token').then(token => {
                if (token) {

                    this.token = token;
                    resolve(this.token);
                    return
                }

                resolve('');
            });
        });
    }


    public set(token: string) {
        if (!token) {
            return;
        }

        this.token = token;
        this.storage.set('access_token', token);
    }

}
