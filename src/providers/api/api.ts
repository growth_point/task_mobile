import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Storage } from "@ionic/storage";
import { BASE_URL } from "../settings/constants";

type GetRequestParams = {
    headers?: HttpHeaders;
    params?: HttpParams;
    reportProgress?: boolean;
    responseType?: 'json';
    withCredentials?: boolean;
}

type PostRequestParams = {
    headers?: HttpHeaders;
    params?: HttpParams;
    reportProgress?: boolean;
    responseType?: 'json';
    withCredentials?: boolean;
}

type PutRequestParams = {
    headers?: HttpHeaders;
    params?: HttpParams;
    reportProgress?: boolean;
    responseType?: 'json';
    withCredentials?: boolean;
}

// we will merge it to request params to simplify interface
type observeResponse = {
    observe: 'response';
}

function extend<T, U>(first: T, second: U): T & U {
    let result = <T & U>{};
    for (let id in first) {
        (<any>result)[ id ] = (<any>first)[ id ];
    }
    for (let id in second) {
        if (!result.hasOwnProperty(id)) {
            (<any>result)[ id ] = (<any>second)[ id ];
        }
    }
    return result;
}

export type Link = {
    href: string;
    title?: string;
    templated?: boolean;
    type?: string;
    name?: string;
    deprecation?: string;
    hreflang?: string;
    profile?: string;
}

type Auth = {
    access_token: string;
    token_type: string;
    expires_in: number;
    scope: string;
    jti: string;
}

// Api is a generic REST Api handler. Set your API url first.
@Injectable()
export class Api {
    baseUrl: string = 'http://' + BASE_URL;
    url: string = this.baseUrl + '/api';

    constructor(public http: HttpClient, public storage: Storage) {
    }

    t() {
        this.http.get(this.baseUrl + '/v2/api-docs').subscribe();
    }
    auth(username: string, password: string, options?: PostRequestParams): Observable<HttpResponse<Auth>> {
        let p = extend<observeResponse, PostRequestParams>({observe: 'response'}, options || {});

        let basicHttpAuth = btoa("mobile" + ":" + "XY7kmzoNzl100");
        p.headers = new HttpHeaders()
            .set('Content-type', 'application/x-www-form-urlencoded; charset=utf-8')
            .set('Authorization', 'Basic ' + basicHttpAuth)

        p.params = new HttpParams()
            .set('grant_type', 'password')
            .set('username', username)
            .set('password', password)


        return this.http.post<Auth>(this.baseUrl + '/oauth/token', p.params.toString(), p);
    }

    get<T>(endpoint: string, options?: GetRequestParams): Observable<HttpResponse<T>> {
        let p = extend<observeResponse, GetRequestParams>({observe: 'response'}, options || {});
        return this.http.get<T>(this.url + '/' + endpoint, p)
    }

    post<T>(endpoint: string, body: any | null, options?: PostRequestParams): Observable<HttpResponse<T>> {
        let p = extend<observeResponse, PostRequestParams>({observe: 'response'}, options || {});
        return this.http.post<T>(this.url + '/' + endpoint, body, p);
    }

    put<T>(endpoint: string, body: any | null, options?: PutRequestParams): Observable<HttpResponse<T>> {
        let p = extend<observeResponse, PutRequestParams>({observe: 'response'}, options || {});
        return this.http.post<T>(this.url + '/' + endpoint, body, p);
    }

    // delete(endpoint: string, options?: RequestOptions) {
    //   return this.http.delete(this.url + '/' + endpoint, options);
    // }

    // patch(endpoint: string, body: any, options?: RequestOptions) {
    //   return this.http.put(this.url + '/' + endpoint, body, options);
    // }
}
