import 'rxjs/add/operator/retry';

import { Injectable } from '@angular/core';

import { Api } from '../api/api';
import { Project } from '../projects/projects';
import { Storage } from "@ionic/storage";
import { Subject } from "rxjs/Subject";
import { Token } from "../api/token";

type LoginResponse = {
  success: boolean;
  user: User;
  labels: Label[];
  projects: Project[];
}

type SignupResponse = {
  success: boolean;
  user: User;
}

export type Label = {
  id: number;
  title: string;
}


export type User = {
  id: number;
  name: string;
  role: string;
}

/**
 * Most apps have the concept of a UserService. This is a simple provider
 * with stubs for login/signup/etc.
 *
 * This UserService provider makes calls to our API at the `login` and `signup` endpoints.
 *
 * By default, it expects `login` and `signup` to return a JSON object of the shape:
 *
 * ```json
 * {
 *   status: 'success',
 *   user: {
 *     // UserService fields your app needs, like "id", "name", "email", etc.
 *   }
 * }
 * ```
 *
 * If the `status` field is not `success`, then an error is detected and returned.
 */
@Injectable()
export class UserService {
  private $user = new Subject<LoginResponse>();

  constructor(public api: Api, public storage: Storage, public token: Token) {
  }

  login(username: string, password: string): Subject<LoginResponse> {
    this.api.auth(username, password).subscribe(res => {
      if (res.body.access_token) {
        this.token.set(res.body.access_token);
      }

      this.api.get<LoginResponse>('users')
        .retry(3)
        .subscribe(res => this.$user.next(res.body), (err) => this.$user.error(err))
    });

    return this.$user
  }

  // /**
  //  * Send a POST request to our login endpoint with the data
  //  * the user entered on the form.
  //  */
  // login(accountInfo: any) {
  //     let seq = this.api.post<LoginResponse>('login', accountInfo, {}).retry(3);
  //
  //     seq
  //         .subscribe(res => {
  //             console.log(1)
  //
  //             // If the API returned a successful response, mark the user as logged in
  //             if (res.status == 200) {
  //                 this._loggedIn(res.body.user);
  //             } else {
  //             }
  //         }, err => {
  //             console.error('ERROR', err);
  //         });
  //
  //     return seq;
  // }
  //
  // /**
  //  * Send a POST request to our signup endpoint with the data
  //  * the user entered on the form.
  //  */
  // signup(accountInfo: any) {
  //     let seq = this.api.post<SignupResponse>('signup', accountInfo);
  //
  //     seq
  //         .subscribe(res => {
  //             // If the API returned a successful response, mark the user as logged in
  //             if (res.status == 200) {
  //                 this._loggedIn(res.body.user);
  //             }
  //         }, err => {
  //             console.error('ERROR', err);
  //         });
  //
  //     return seq;
  // }

  /**
   * Log the user out, which forgets the session
   */
  logout() {
    this.$user.next(null);
  }


  // /**
  //  * Process a login/signup response to store user data
  //  */
  // _loggedIn(user: User) {
  //     console.log(user)
  //     this._user = user;
  // }
}
