import { Injectable, OnDestroy } from '@angular/core';

import { Api, Link } from '../api/api';
import { Storage } from "@ionic/storage";
import { BehaviorSubject } from "rxjs/BehaviorSubject";

export type Label = {
    id: number;
    title: string;
    _links: {
        self: Link;
        tasks: Link;
        user: Link;
    }
}

@Injectable()
export class LabelsService implements OnDestroy {
    private $labels = new BehaviorSubject<Label[]>([]);
    private labels: Label[] = [];
    private syncTimeout = 30000;
    private intervalId: number;
    private tmpId = 0;

    constructor(public api: Api,
                public storage: Storage) {

        // this.intervalId = setInterval(this._sync, this.syncTimeout);
    }

    ngOnDestroy() {
        clearInterval(this.intervalId);
    }

    private fromStorage(list: Label[]) {
        this.labels = list;
        this.$labels.next(this.labels);
    }

    private fromServer(list: Label[]) {
        //TODO: must be merge logic here
        this.labels = list;
        this.broadcast();
    }

    private findAndReplace(id: number, label: Label) {
        this.labels[ this.labels.findIndex(i => i.id == id) ] = label;
        this.broadcast();
    }

    private broadcast() {
        this.$labels.next(this.labels);
        this.storage.set('labels', this.labels);
    }

    all(): BehaviorSubject<Label[]> {
        if (this.labels.length > 0) {
            return this.$labels;
        }

        this.storage.get('labels').then(res => {
            if (!res) {
                this._sync();
                return;
            }

            this.fromStorage(res);
        });

        if (!this.intervalId) {
            this.intervalId = setInterval(this._sync.bind(this), this.syncTimeout);
        }

        return this.$labels;
    }

    private _sync() {
        type LabelsResponse = {
            _embedded: {
                labels: Label[];
            };
            _links: {
                self: Link;
                profile: Link;
            };
        }

        this.api.post<LabelsResponse>('labels', {'timestamp': (new Date).toTimeString()})
        // .retry(5)
            .subscribe(resp => {
                if (resp.body._embedded.labels instanceof Array) {
                    this.fromServer(resp.body._embedded.labels);
                }
            });
    }

    add(label: Label) {
        let tmpId = this.tmpId--;
        label.id = tmpId;

        this.findAndReplace(tmpId, label);

        type Response = {
            label: Label
        }
        this.api.post<Response>('label', {'label': label}).subscribe(resp => {
            this.findAndReplace(tmpId, resp.body.label);
        });
    }

    update(label: Label) {
        this.findAndReplace(label.id, label);

        //TODO: label may have negative Id. Need to sync it in background?
        type Response = {
            label: Label
        }
        this.api.put<Response>('label', {'label': label}).subscribe(resp => {
            this.findAndReplace(label.id, resp.body.label);
        });
    }

}
