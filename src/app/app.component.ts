import { Component, ViewChild } from '@angular/core';
import { Platform, App, Nav } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { Token } from "../providers/api/token";

@Component({
  template: `
    <ion-menu [content]="content">
      <ion-header>
        <ion-toolbar>
          <ion-title>Menu</ion-title>
        </ion-toolbar>
      </ion-header>

      <ion-content>
        
      </ion-content>

    </ion-menu>

    <!-- Disable swipe-to-go-back because it's poor UX to combine STGB with side menus -->
    <ion-nav [root]="rootPage" #content swipeBackEnabled="false"></ion-nav>
  `
})
export class MyApp {
  @ViewChild(Nav) nav;
  rootPage: any;

  constructor(platform: Platform,
    statusBar: StatusBar,
    splashScreen: SplashScreen,
    public app: App,
    public token: Token) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }

  ngOnInit() {
    this.token.has().then(has => {
        this.rootPage = has ? 'HomePage': 'LoginPage';
    })

    this.nav.viewDidEnter.subscribe((view) => {
      switch (view.instance.constructor.name) {
        case 'HomePage':
          // this.page.set('home');
          break;
        case 'TasksPage':
          // this.page.set('tasks');
          break;
      }
    });

    //TODO: check, maybe need to put it to ngAfterViewInit
    /*
    this.page.get().subscribe(page => {
      console.log('get page', page)
      switch (page) {
        case 'tasks':
          this.nav.push(TasksPage, {}, {animate: false});
          break;
      }
    })
    */
  }
}
