import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { IonicStorageModule } from '@ionic/storage';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { HttpClientModule } from '@angular/common/http';

import { Token, Api } from '../providers/providers';

import { JwtModule, JWT_OPTIONS } from '@auth0/angular-jwt';
import { BASE_URL } from "../providers/settings/constants";


export function jwtOptionsFactory(provider) {
  return {
    tokenGetter: () => {
      return provider.get();
    },
    whitelistedDomains: ['localhost:8100', 'localhost:8000', '127.0.0.1:8000', BASE_URL]
  }
}

@NgModule({
  declarations: [
    MyApp,
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    HttpClientModule,
    JwtModule.forRoot({
      jwtOptionsProvider: {
        provide: JWT_OPTIONS,
        useFactory: jwtOptionsFactory,
        deps: [Token]
      }
    })

  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    Token,
    Api
  ]
})
export class AppModule {
}
