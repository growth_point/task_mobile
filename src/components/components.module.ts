import { NgModule } from '@angular/core';

import { TaskComponent } from './task/task';

@NgModule({
  declarations: [TaskComponent],
  imports: [],
  exports: [TaskComponent]
})
export class ComponentsModule {
}
