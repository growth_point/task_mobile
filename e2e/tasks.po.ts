import { browser, ExpectedConditions, $, $$ } from 'protractor';
import Util from "./utils";
import { promise } from "selenium-webdriver";
import OldPromise = promise.Promise;

export class TasksPage {
  protected tasks = $('tasks-list ion-list');
  protected firstTask = this.tasks.$$('ion-item-sliding').first();
  protected taskTitle = this.firstTask.$$('ion-label').first();
  protected btnDone = this.firstTask.$$('button.done').first();
  protected btnEdit = this.firstTask.$$('button.edit').first();
  protected btnCreate = this.tasks.$$('button.create').first();

  navigateTo(destination): OldPromise<any> {
    browser.get(destination);
    return this.waitForTaskAppear();
  }

  getPageTitle(): OldPromise<string> {
    return browser.getTitle();
  }

  getTaskTitle(): OldPromise<string> {
    return this.firstTask.getText()
  }

  isTaskClickable(): OldPromise<boolean> {
    return browser.wait(ExpectedConditions.elementToBeClickable(this.firstTask));
  }

  waitForTaskAppear(): OldPromise<any> {
    return browser.wait(ExpectedConditions.presenceOf(this.firstTask));
  }

  waitForSlidingClose(): OldPromise<string> {
    return browser.wait(ExpectedConditions.invisibilityOf(this.btnDone));
  }

  swipe(): Promise<any> {
    return Util.swipeLeft(this.firstTask);
  }

  done(): Promise<void> {
    return Util.click(this.btnDone);
  }

  clickCreateTask() {
    return Util.click(this.btnCreate);
  }


}