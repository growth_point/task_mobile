import { TasksPage } from './tasks.po';
import { ProjectsPage } from './projects.po';
import './utils';

describe('App', () => {
  let projectsPage: ProjectsPage;
  let tasksPage: TasksPage;

  beforeEach(() => {
    projectsPage = new ProjectsPage();
    tasksPage = new TasksPage();
  });

  describe('tasks page', () => {
    beforeEach(() => {
      projectsPage.open().then(() => {
        projectsPage.clickProject().then(() => {
          tasksPage.waitForTaskAppear();
        })
      });
    });

    it('should have a title saying Today', () => {
      tasksPage.getPageTitle().then(title => {
        expect(title).toEqual('Today');
      });
    });

    it('tasks rows clickable', () => {
      tasksPage.isTaskClickable().then(clickable => {
        expect(clickable).toBeTruthy();
      });
    });

    it('tasks should disappear after click "done"', () => {
      Promise.all([tasksPage.getTaskTitle, tasksPage.swipe]).then(results => {
        let title = results[0];

        projectsPage.done().then(() => {
          expect(tasksPage.getTaskTitle()).not.toContain(title);
        });
      })
    });

    it('can click "create project" button ', () => {
      tasksPage.clickCreateTask().then(() => {
        tasksPage.getPageTitle().then(title => {
          expect(title).toEqual('Create Task');
        });
      });
    });

  })
});