import { ProjectsPage } from './projects.po';
import './utils';

describe('App', () => {
  let projectsPage: ProjectsPage;

  beforeEach(() => {
    projectsPage = new ProjectsPage();
  });

  describe('tasks page', () => {
    beforeEach(() => {
      projectsPage.open();
    });

    it('should have a title saying Today', () => {
      projectsPage.getPageTitle().then(title => {
        expect(title).toEqual('Today');
      });
    });

    it('project rows clickable', () => {
      projectsPage.isProjectClickable().then(clickable => {
        expect(clickable).toBeTruthy();
      });
    });

    it('project should disappear after click "done"', () => {
      Promise.all([projectsPage.getProjectTitle, projectsPage.swipe]).then(results => {
        let title = results[0];

        projectsPage.done().then(() => {
          expect(projectsPage.getProjectTitle()).not.toContain(title);
        });
      })
    });

    it('can click "create project" button ', () => {
      projectsPage.clickCreateProject().then(() => {
        projectsPage.getPageTitle().then(title => {
          expect(title).toEqual('Create Project');
        });
      });
    });

  })
});