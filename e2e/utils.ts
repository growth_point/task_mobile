import { browser, ElementFinder } from "protractor";
import { ILocation, promise } from "selenium-webdriver";

export default class Utils {
  static click(el: ElementFinder): Promise<void> {
    return new Promise<void>((resolve, reject) => {
      browser.actions().click(el).perform().then(resolve).catch(reject)
    });
  }

  static swipeLeft(elem: ElementFinder): Promise<any> {
    return Utils.toEs6(browser.actions()
      .mouseDown(elem)
      .mouseMove({x: -50, y: 0}) // yes, we need few moves, with single mouseMove ionic can't catch swipe event
      .mouseMove({x: -50, y: 0})
      .mouseMove({x: -50, y: 0})
      .mouseUp()
      .perform()
    );
  }

  static getCenterOfElement(elem: ElementFinder): Promise<ILocation> {
    return new Promise<ILocation>((resolve, reject) => {
      elem.getLocation().then(loc => {
        elem.getSize().then(size => {
          resolve({
            x: loc.x + (size.width / 2),
            y: loc.y + (size.height / 2),
          })
        })
      });
    });
  }

  static proxyLogsFromBrowser() {
    setInterval(() => {
      browser.manage().logs().get('browser').then((browserLog) => {
        browserLog.forEach(i => {
          console.log('browser log: ', i.message);
        });
      });
    }, 100);

    /*
            browser.manage().logs().get('browser').then(function(browserLogs) {
              // browserLogs is an array of objects with level and message fields
              browserLogs.forEach(function(log){
                if (log.level.value > 900) { // it's an error log
                  console.log('Browser console error!');
                  console.log(log.message);
                }
              });
            });
  */
  }

  static toEs6<T>(old: promise.Promise<T>): Promise<T> {
    return new Promise((resolve, reject) => {
      old.then(resolve)
    })
  }
}

