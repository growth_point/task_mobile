import { browser, ExpectedConditions, $, $$ } from 'protractor';
import Util from "./utils";
import { promise } from "selenium-webdriver";
import OldPromise = promise.Promise;

export class ProjectsPage {
  protected projects = $('tasks-list ion-list');
  protected firstProject = this.projects.$$('ion-item-sliding').first();
  protected projectTitle = this.firstProject.$$('ion-label').first();
  protected btnDone = this.firstProject.$$('button.done').first();
  protected btnEdit = this.firstProject.$$('button.edit').first();
  protected btnCreate = this.projects.$$('button.create').first();

  open(): OldPromise<any> {
    browser.get('/');
    return this.waitForProjectAppear();
  }

  getPageTitle(): OldPromise<string> {
    return browser.getTitle();
  }

  getProjectTitle(): OldPromise<string> {
    return this.firstProject.getText()
  }

  isProjectClickable(): OldPromise<boolean> {
    return browser.wait(ExpectedConditions.elementToBeClickable(this.firstProject));
  }

  waitForProjectAppear(): OldPromise<any> {
    return browser.wait(ExpectedConditions.presenceOf(this.firstProject));
  }

  waitForProjectSlidingClose(): OldPromise<string> {
    return browser.wait(ExpectedConditions.invisibilityOf(this.btnDone));
  }

  swipe(): Promise<any> {
    return Util.swipeLeft(this.firstProject);
  }

  clickProject(): Promise<void> {
    return Util.click(this.firstProject);
  }

  done(): Promise<void> {
    return Util.click(this.btnDone);
  }

  clickCreateProject() {
    return Util.click(this.btnCreate);
  }


}